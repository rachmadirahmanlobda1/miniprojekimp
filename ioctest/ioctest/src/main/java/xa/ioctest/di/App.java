package xa.ioctest.di;

import org.springframework.context.annotation.Bean;

public class App {

	public static void main(String[] args) {
		
		EmailService emailService = new  EmailServiceImpl();
		new UserView().show(emailService);

	}

}
