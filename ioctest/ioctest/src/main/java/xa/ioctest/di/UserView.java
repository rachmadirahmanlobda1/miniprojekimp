package xa.ioctest.di;

import java.util.Scanner;

public class UserView {
	private Scanner scan = new Scanner(System.in);
		
	
	void show(EmailService emailService) { //melakukan injec emialserimp ke metod show
		System.out.println("to :");
		String to = scan.nextLine();
		
		System.out.println("body :");
		String body = scan.nextLine();
		
		System.out.println("Kirim  [y/t]:");
		String option = scan.nextLine();
		
		if(option.equalsIgnoreCase("y")) {
			emailService.send(to, body);
		}
	
	}
}
