package xa.ioctest.di;

public interface EmailService {
	void send(String to, String body);
}
