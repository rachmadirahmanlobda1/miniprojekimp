package xa.projekmini.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;

import org.springframework.lang.Nullable;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;

@Entity
@Table(name = "category")
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long Id;

	@NotNull
	@Column(name = "initial", length = 10)
	private String Initial;

	@NotNull
	@Column(name = "name", length = 50)
	private String Name;

	@NotNull
	@Column(name = "active")
	private Boolean Active;

	@NotNull
	@Column(name = "create_by", length = 50)
	private String Create_by;

	@NotNull
	@Column(name = "create_date")
	private LocalDateTime Created_date;

	@Nullable
	@Column(name = "modify_by", length = 50)
	private String Modify_by;

	@Nullable
	@Column(name = "modify_date")
	private LocalDateTime Modify_date;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getInitial() {
		return Initial;
	}

	public void setInitial(String initial) {
		Initial = initial;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Boolean getActive() {
		return Active;
	}

	public void setActive(Boolean active) {
		Active = active;
	}

	public String getCreate_by() {
		return Create_by;
	}

	public void setCreate_by(String create_by) {
		Create_by = create_by;
	}

	public LocalDateTime getCreated_date() {
		return Created_date;
	}

	public void setCreated_date(LocalDateTime created_date) {
		Created_date = created_date;
	}

	public String getModify_by() {
		return Modify_by;
	}

	public void setModify_by(String modify_by) {
		Modify_by = modify_by;
	}

	public LocalDateTime getModify_date() {
		return Modify_date;
	}

	public void setModify_date(LocalDateTime modify_date) {
		Modify_date = modify_date;
	}

}