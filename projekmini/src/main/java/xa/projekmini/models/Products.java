package xa.projekmini.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import groovyjarjarantlr4.v4.runtime.misc.Nullable;

import javax.persistence.GenerationType;

@Entity
@Table(name = "products")
public class Products {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@ManyToOne
	@JoinColumn(name = "varianId", insertable = false, updatable = false)
	public Variants varian;

	@Column(name = "varianId")
	private long VarianId;

	@NotNull
	@Column(name = "initial", length = 10, unique = true)
	private String Initial;

	@NotNull
	@Column(name = "name", length = 50, unique = true)
	private String Name;

	@Nullable
	@Column(name = "description", length = 500)
	private String Description;

	@NotNull
	@Column(name = "price")
	private Double Price;

	@NotNull
	@Column(name = "stock")
	private int Stock;

	@NotNull
	@Column(name = "active")
	private boolean Active;

	@NotNull
	@Column(name = "CreateBy")
	private String Create_by;

	@NotNull
	@Column(name = "CreateDate")
	private LocalDateTime Create_date;

	@Nullable
	@Column(name = "modify_by", length = 50)
	private String Modify_by;

	@Nullable
	@Column(name = "modify_date")
	private LocalDateTime Modify_date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVarianId() {
		return VarianId;
	}

	public void setVarianId(long varianId) {
		VarianId = varianId;
	}

	public String getInitial() {
		return Initial;
	}

	public void setInitial(String initial) {
		Initial = initial;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Double getPrice() {
		return Price;
	}

	public void setPrice(Double price) {
		Price = price;
	}

	public int getStock() {
		return Stock;
	}

	public void setStock(int stock) {
		Stock = stock;
	}

	public boolean isActive() {
		return Active;
	}

	public void setActive(boolean active) {
		Active = active;
	}

	public String getCreate_by() {
		return Create_by;
	}

	public void setCreate_by(String create_by) {
		Create_by = create_by;
	}

	public LocalDateTime getCreate_date() {
		return Create_date;
	}

	public void setCreate_date(LocalDateTime create_date) {
		Create_date = create_date;
	}

	public String getModify_by() {
		return Modify_by;
	}

	public void setModify_by(String modify_by) {
		Modify_by = modify_by;
	}

	public LocalDateTime getModify_date() {
		return Modify_date;
	}

	public void setModify_date(LocalDateTime modify_date) {
		Modify_date = modify_date;
	}

	/*public Products(long id, String name, String description, double price, int stock) {
		super();
		this.id = id;
		Name = name;
		Description = description;
		Price = price;
		Stock = stock;
	}

	@Override
	public String toString() {
		return "Products [id=" + id + ", Name=" + Name + ", Description=" + Description + ", Price=" + Price
				+ ", Stock=" + Stock + "]";
	}

	public static List<String> newList(){
		// TODO Auto-generated method stub
		return Arrays.asList("apel","alpukat");
	}
	*/

}