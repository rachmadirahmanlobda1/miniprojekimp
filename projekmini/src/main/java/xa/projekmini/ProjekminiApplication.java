package xa.projekmini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjekminiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjekminiApplication.class, args);
	}

}
