package xa.projekmini.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import xa.projekmini.models.Category;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {

}
