package xa.projekmini.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import xa.projekmini.models.Products;

@Repository
public interface ProductsRepo extends JpaRepository<Products, Long> { // Variant ambil di model{
	@Query(value = "SELECT * FROM products p WHERE LOWER(p.Name) LIKE %:textsearch% OR LOWER (p.Description) LIKE %:textsearch%", nativeQuery = true)
    java.util.List<Products> seacProducts(@Param("textsearch") String textsearch);
}
