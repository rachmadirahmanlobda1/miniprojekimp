package xa.projekmini.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import xa.projekmini.models.Variants;

@Repository
public interface VarianstRepo extends JpaRepository<Variants, Long> { // Variant ambil di model
    @Query("FROM Variants WHERE CategoryId = ?1")
    List<Variants> findByCategoryId(Long CategoryId);
}