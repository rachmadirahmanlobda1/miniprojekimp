package xa.projekmini.controllers;

import java.io.IOException;
import java.security.Provider.Service;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.lowagie.text.DocumentException;

import xa.projekmini.exporters.ProductExpoerter;
import xa.projekmini.models.Products;
import xa.projekmini.repositories.CategoryRepo;
import xa.projekmini.repositories.VarianstRepo;
import xa.projekmini.service.ProductService;
import xa.projekmini.repositories.ProductsRepo;

@Controller
@RequestMapping(value = "/products/")
public class ProductsController {
	@Autowired
	VarianstRepo varianrepo;
	@Autowired
	CategoryRepo catrepo;
	@Autowired
	ProductsRepo producrepo;

	@GetMapping(value = "index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/products/index");
		return view;
	}
	@GetMapping(value = "form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/products/form");
		return view;
	}
	@GetMapping(value = "stream")
	public ModelAndView stream() {
		ModelAndView view = new ModelAndView("/products/stream");
		return view;
	}

@Autowired ProductService service;
	
	@GetMapping(value="exportproduct")
	public void exportproduct(HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("application/pdf");
		DateFormat dateformat = new SimpleDateFormat("yyyymmmddHHmmss");
		String currentdate = dateformat.format(new Date());
		
		String headerkey = "Content-Disposition";
		String headerValue = "attachment; filename=product_"+ currentdate + ".pdf";
		response.setHeader(headerkey, headerValue);
		
		List<Products> listproduct = service.listAll();
		ProductExpoerter exporter = new ProductExpoerter(listproduct);
		exporter.export(response);
	}

}
