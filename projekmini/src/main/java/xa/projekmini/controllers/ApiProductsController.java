package xa.projekmini.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xa.projekmini.models.Products;
import xa.projekmini.repositories.ProductsRepo;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class ApiProductsController {
    @Autowired
    ProductsRepo prorepo;

    @GetMapping("/products")
    public ResponseEntity<List<Products>> getAllPro() {
        try {
            List<Products> products = this.prorepo.findAll();
            /*List<Products> products = newList();
            Stream<Products> products = (Stream<Products>) this.prorepo.findAll();
            products.stream()
          .filter((value)->value.startsWith("A"))
          .forEach(System.out::print);*/
            return new ResponseEntity<List<Products>>(products, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Products>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Products> getProById(@PathVariable Long id) {
        try {
            Products products = this.prorepo.findById(id).orElse(null);
            return new ResponseEntity<Products>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Products>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/insertProducts")
    public ResponseEntity<Products> insertProducts(@RequestBody Products products) {
        try {
            this.prorepo.save(products);
            return new ResponseEntity<Products>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Products>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editProducts/{id}")
    public ResponseEntity<Products> editProducts(@RequestBody Products products, @PathVariable Long id) {
        try {
            products.setId(id);
            this.prorepo.save(products);
            return new ResponseEntity<Products>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Products>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/deleteProducts/{id}")
    public ResponseEntity<Products> deleteProducts(@PathVariable Long id) {
        try {
            this.prorepo.deleteById(id);
            return new ResponseEntity<Products>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Products>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/products/search/{textsearch}")
    public ResponseEntity<List<Products>> getAllproduct(@PathVariable("textsearch") String textsearch) {
        try {
            List<Products> products = this.prorepo.seacProducts(textsearch);
            return new ResponseEntity<List<Products>>(products, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Products>>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("listproduct")
    public ResponseEntity<List<Products>> listproducts(){
    	try {
			List<Products> listpro = this.prorepo.findAll();
			List<Products> listsort = new ArrayList<Products>();
			listsort =(List<Products>) listpro.stream().sorted((f, s)-> f.getPrice().compareTo(s.getPrice())).collect(Collectors.toList());
			return new ResponseEntity<>(listsort, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
    }
    
    @GetMapping("listpelanngan")
    public ResponseEntity<List<Products>> listKota(){
    	try {
			List<Products> listpro = this.prorepo.findAll();
			List<Products> listsort = new ArrayList<Products>();
			listsort =(List<Products>) listpro.stream().filter(pelang -> {
                System.out.println("filter car " + pelang);
                return pelang.getPrice() <= 10000000;
            }).collect(Collectors.toList());
			return new ResponseEntity<>(listsort, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
    }
    
}
