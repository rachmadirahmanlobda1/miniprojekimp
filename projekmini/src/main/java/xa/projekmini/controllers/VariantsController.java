package xa.projekmini.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


import xa.projekmini.models.Variants;
import xa.projekmini.models.Category;
import xa.projekmini.repositories.CategoryRepo;
import xa.projekmini.repositories.VarianstRepo;

@Controller
@RequestMapping(value = "/variants/")
public class VariantsController {
	@Autowired
	VarianstRepo varianrepo;
	@Autowired
	CategoryRepo catrepo;

	@GetMapping(value = "index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/variants/index");
		List<Variants> listvan = this.varianrepo.findAll();
		view.addObject("listvan", listvan);
		return view;
	}

	@GetMapping(value = "form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/variants/form"); /* ini adalah tempat di web /ca/form */
		Variants variants = new Variants();
		view.addObject("variants", variants); // membuat variants
		List<Category> category = this.catrepo.findAll(); // this.catrepo.findAll();
		view.addObject("category", category);
		return view;
	}

	@PostMapping(value = "save")
	public ModelAndView save(@ModelAttribute Variants variants, BindingResult result) {
		if (!result.hasErrors()) {
			variants.setCreate_by("Rahmadi");
			this.varianrepo.save(variants);
		}
		return new ModelAndView("redirect:/variants/index");
	}

	@GetMapping(value = "/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/variants/form");
		Variants variants = this.varianrepo.findById(id).orElse(null);
		view.addObject("variants", variants);
		List<Category> category = this.catrepo.findAll(); // this.catrepo.findAll();
		view.addObject("category", category);
		return view;

	}

	@GetMapping(value = "/delete/{id}")
	public ModelAndView deleteform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/variants/delete"); // sesuai nama file
		Variants variants = this.varianrepo.findById(id).orElse(null);
		view.addObject("variants", variants); // memanggil pada file model untuk delete <table th:each="item, iter:
												// ${category}">
		return view;
	}

	@GetMapping(value = "/del/{id}")
	public ModelAndView del(@PathVariable("id") Long id) {
		if (id != null) {
			this.varianrepo.deleteById(id);
		}
		return new ModelAndView("redirect:/variants/index");
	}
}
