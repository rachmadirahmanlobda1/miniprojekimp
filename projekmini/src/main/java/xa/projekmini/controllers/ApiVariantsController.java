package xa.projekmini.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lowagie.text.DocumentException;

import xa.projekmini.repositories.VarianstRepo;
import xa.projekmini.service.VariantsService;
import xa.projekmini.exporters.varianExpoerter;
import xa.projekmini.models.Variants;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class ApiVariantsController {
    @Autowired
    VarianstRepo varepo;

    @GetMapping("/variants")
    public ResponseEntity<List<Variants>> getAllCat() {
        try {
            List<Variants> varianst = this.varepo.findAll();
            return new ResponseEntity<List<Variants>>(varianst, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Variants>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/variants/{id}")
    public ResponseEntity<Variants> getCatById(@PathVariable Long id) {
        try {
            Variants variants = this.varepo.findById(id).orElse(null);
            return new ResponseEntity<Variants>(variants, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Variants>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/insertVariants")
    public ResponseEntity<Variants> insertVariants(@RequestBody Variants variants) {
        try {
            this.varepo.save(variants);
            return new ResponseEntity<Variants>(variants, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Variants>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editVariants/{id}")
    public ResponseEntity<Variants> editVariants(@RequestBody Variants variants, @PathVariable Long id) {
        try {
            variants.setId(id);
            this.varepo.save(variants);
            return new ResponseEntity<Variants>(variants, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Variants>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/deleteVariants/{id}")
    public ResponseEntity<Variants> deleteVariants(@PathVariable Long id) {
        try {
            this.varepo.deleteById(id);
            return new ResponseEntity<Variants>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Variants>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getvarbycatid/{id}")
    public ResponseEntity<?> getvaCatById(@PathVariable Long id) {
        try {
            List<Variants> variants = this.varepo.findByCategoryId(id);
            if (variants != null) {
                return new ResponseEntity<>(variants, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body("CategoryID dengan ID" + id + "tidak ditemukan");
            }

        } catch (Exception e) {
            return new ResponseEntity<Variants>(HttpStatus.NO_CONTENT);
        }
    }
    
@Autowired VariantsService service;
	
	@GetMapping(value="exportvarian")
	public void exportproduct(HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("application/pdf");
		DateFormat dateformat = new SimpleDateFormat("yyyymmmddHHmmss");
		String currentdate = dateformat.format(new Date());
		
		String headerkey = "Content-Disposition";
		String headerValue = "attachment; filename=varian_"+ currentdate + ".pdf";
		response.setHeader(headerkey, headerValue);
		
		List<Variants> listvarian = service.listAll();
		varianExpoerter exporter = new varianExpoerter(listvarian);
		exporter.export(response);
	}
}
