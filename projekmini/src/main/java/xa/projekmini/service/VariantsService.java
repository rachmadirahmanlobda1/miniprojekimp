package xa.projekmini.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xa.projekmini.models.Variants;
import xa.projekmini.repositories.VarianstRepo;

@Service
@Transactional

public class VariantsService {
	@Autowired VarianstRepo variantsrepo;
	
	public List<Variants> listAll() {
		return this.variantsrepo.findAll();
	}
}
