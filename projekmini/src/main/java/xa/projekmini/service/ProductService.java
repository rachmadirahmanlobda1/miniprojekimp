package xa.projekmini.service;

import java.util.List;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xa.projekmini.models.Products;
import xa.projekmini.repositories.ProductsRepo;

@Service
@Transactional
public class ProductService {

	@Autowired ProductsRepo productrepo;
	
	
	public List<Products>  listAll() {
		/*try(Stream<Products> stream = service.streamAll) {
	        return stream.collect(Collectors.toList())
	    };*/
		return this.productrepo.findAll();
	}



}